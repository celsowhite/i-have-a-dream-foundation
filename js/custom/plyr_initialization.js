(function ($) {

    $(document).ready(function () {

        "use strict"; 

        /*================================= 
		Plyr Initialization
		=================================*/

		// Capture all of the plyr instances on the page and initialize them.

		const videoPlayers = plyr.setup();

        // Use the plyr video functionality to control the Flexslider instance.
        // When a user plays a plyr video within a Flexslider then the slider should stop auto rotating.

        if(videoPlayers) {

            videoPlayers.forEach(player => {

                // Listen for the play event.

                player.on('play', function () {

                    // Find the parent Flexslider instance and pause it.

                    const flexsliderInstance = $(this).closest('.flexslider');
                    flexsliderInstance.flexslider('stop');

                });

            });

        }
    
    });

})(jQuery);