(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		HEADER SCROLLED TRANSFORMATION
		=================================*/

		const mainHeader = document.querySelector('header.main_header');

		window.addEventListener('scroll', function() {
			const scrollPosition = window.scrollY;
			if(scrollPosition > 100) {
				mainHeader.classList.add('scrolled');
			}
			else {
				mainHeader.classList.remove('scrolled');
			}
		});

		/*================================= 
		SEARCH REVEAL
		=================================*/

		var searchToggle = document.querySelector('.search_toggle');
		var searchForm = document.querySelector('.ihdf_search');
		var searchInput = document.querySelector('.ihdf_search input');

		function toggleSearch() {
			searchForm.classList.toggle('open');
			if(searchForm.classList.contains('open')) {
				searchInput.focus();
				searchInput.value = '';
			}
		}

		searchToggle.addEventListener('click', toggleSearch);
			
		/*================================= 
		MOBILE NAVIGATION REVEAL
		=================================*/

		var menuIcon = $('.menu_icon');
		var mobileNav = $('nav.mobile_navigation ul.menu');

		menuIcon.click(function(){
			mobileNav.toggleClass('open');
		});

		/*================================= 
		MOBILE NAVIGATION SUBMENU REVEAL
		=================================*/

		var mobileMenuLink = $('.mobile_navigation li.menu-item-has-children');

		mobileMenuLink.click(function(){
			var subMenu = $(this).find('ul.sub-menu');
			subMenu.toggleClass('open');
		});

	});

})(jQuery);