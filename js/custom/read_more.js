(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		READ MORE CONTENT
		=================================*/

		// Save the containers that need to be transformed to read more.
		// By default in the php all content is displayed on page load

		const contentAreas = document.querySelectorAll('.read_more_container');

		Array.from(contentAreas).forEach(content => {

			// Save all p child nodes of the container

			const childNodes = content.querySelectorAll('p');

			const childNodesArray = Array.from(childNodes);

			// Save the first paragraph and transform the original array to just have the additional paragraphs

			const firstParagraph = childNodesArray.splice(0,1);

			if(firstParagraph[0]) {

				// Get the first paragraph content

				const firstParagraphText = firstParagraph[0].innerHTML;

				// Create an HTML string of the additional paragraphs

				const moreParagraphs = childNodesArray.map(node => {
					return '<p>' + node.innerHTML + '</p>';
				}).join('');

				// If the there are more paragraphs then set a read more link

				let readMoreLink = '';

				if(moreParagraphs !== '') {
					readMoreLink = '<a class="read_more_link underlined_link">Read More</a>';
				}

				content.innerHTML = '<p>' + firstParagraphText + '</p>' + '<div class="read_more_content">' + moreParagraphs + '</div>' + readMoreLink;

			}

		});

		/*================================= 
		READ MORE LINK
		=================================*/

		const readMoreLinks = document.querySelectorAll('.read_more_link');

		// On click of the read more link show/hide the content
		
		function showMoreContent() {

			const closestReadMoreContent = this.previousSibling;

			if(this.innerHTML === 'Read More') {
				// Show the associated read more content
				closestReadMoreContent.style.display = 'block';
				// Change the link text to show less
				this.innerHTML = 'Read Less';
			}

			else {
				// Hide the associated read more content
				closestReadMoreContent.style.display = 'none';
				// Change the link text to show more
				this.innerHTML = 'Read More';
			}

		};

		Array.from(readMoreLinks).forEach(link => {
			link.addEventListener('click', showMoreContent);
		});

	});

})(jQuery);