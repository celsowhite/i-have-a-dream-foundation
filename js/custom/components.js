(function($) {

	$(document).ready(function() {

	// Call these functions when the html has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

		/*================================= 
		PAGE LOADING
		=================================*/

		const mainContent = $('.main_content');

		const loadingSpinner = $('.loading_spinner');

		setTimeout(function(){
			loadingSpinner.fadeOut(function(){
				mainContent.addClass('loaded');
			});
		}, 1000);

		/*================================= 
		SUBMIT YOUR DREAM FORM POPUP
		=================================*/

		const submitYourDreamButton = document.querySelector('.submit_your_dream');

		const submitYourDreamPopup = document.querySelector('.popup_form_container');

		const closeIcon = $('.popup_form_container .close_icon');

		if(submitYourDreamButton) {
			submitYourDreamButton.addEventListener('click', function(){
				submitYourDreamPopup.classList.add('open');
			});
		}

		closeIcon.click(function(){
			$(this).closest('.popup_form_container').removeClass('open');
		});

		/*================================= 
		SMOOTH SCROLL
		=================================*/

		$('.smooth_scroll').click(function() {

			// Save our breakpoint for where the header is no longer sticky.
			// Use that breakpoint to adjust the offset distance.

			const tabletLandscapeBreakpoint = window.matchMedia("(min-width: 1024px)");

			// Get the target element & set normal offset

			let target = $(this).attr('data-target');
			let offsetDistance = -20;

			// If above our breakpoint then make sure to set a greater offset scroll to accomodate for the sticky header.

			if (tabletLandscapeBreakpoint.matches) {
				offsetDistance = -100;
			}

			target = $('#' + target);
			$('html, body').animate({
			  scrollTop: target.offset().top + offsetDistance,
			}, 1000);
			return false;
		});

		/*================================= 
		SOCIAL SHARE
		=================================*/

		// Facebook

		$( "body" ).on( "click", "a.fb_share", function(e){

			e.preventDefault();

			var loc = $(this).attr('href');

			window.open('http://www.facebook.com/sharer.php?u=' + loc,'facebookwindow','height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

		});

		// Twitter

		$( "body" ).on( "click", "a.twitter_share", function(e){

		    e.preventDefault();

		    var loc = $(this).attr('href');

		    window.open('http://twitter.com/share?url=' + loc, 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

		});

		$( "body" ).on( "click", "a.tweet_this", function(e){

		    e.preventDefault();

		    var text = encodeURIComponent($(this).attr('data-text'));

		    window.open('http://twitter.com/share?url=d&text=' + text, 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

		});

		// LinkedIn

		$( "body" ).on( "click", "a.linkedin_share", function(e){

			e.preventDefault();

			var loc = $(this).attr('href');

			var title = encodeURIComponent($(this).attr('title'));

			var excerpt  = encodeURIComponent($(this).attr('excerpt'));

			window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + loc + '&title=' + title);

		});

		/*================================= 
		DONATE FLOATER
		=================================*/

		const donateFloater = document.querySelector('.donate_floater');

		function showDonateFloater() {
			if(window.scrollY > 100) {
				donateFloater.classList.add('visible');
			}
			else {
				donateFloater.classList.remove('visible');
			}
		}

		if(donateFloater) {
			window.addEventListener('scroll', debounce(showDonateFloater, 100));
		}

		/*================================= 
		MAILCHIMP AJAX
		=================================*/

		// Set up form variables

		var mailchimpForm = $('.mc-embedded-subscribe-form');

		// On submit of the form send an ajax request to mailchimp for data.

		mailchimpForm.submit(function(e){

			// Set variables for this specific form

			var that = $(this);
			var mailchimpSubmit = $(this).find('input[type=submit]');
			var errorResponse = $(this).closest('.mc_embed_signup').find('.mce-error-response');
			var successResponse = $(this).closest('.mc_embed_signup').find('.mce-success-response');

			// Make sure the form doesn't link anywhere on submit.

			e.preventDefault();

			// JQuery AJAX request http://api.jquery.com/jquery.ajax/

			$.ajax({
			  method: 'GET',
			  url: that.attr('action'),
			  data: that.serialize(),
			  dataType: 'jsonp',
			  success: function(data) {
				// If there was an error then show the error message.
				if (data.result === 'error') {
					// Hide the first few characters int the error message string which display the error code and hyphen.
					var messageWithoutCode = data.msg.slice(3);
					errorResponse.text(messageWithoutCode).fadeIn(300).delay(3000).fadeOut(300);
				}
				// If success then show message
				else {
					successResponse.text('Success! Please check your email for a confirmation message.').fadeIn(300).delay(3000).fadeOut(300);
				}
			  }
			});
		});

		/*================================= 
		FITVIDS
		=================================*/

		/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

		$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

		/*=== Target div for fitVids ===*/

		$(".video_embed").fitVids();

	});

})(jQuery);


