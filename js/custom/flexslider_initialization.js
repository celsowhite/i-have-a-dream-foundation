(function($) {

	$(window).load(function() {

	// Call these functions when all of the html and content has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

		/*================================= 
		MAIN SLIDER
		=================================*/

		$('.ihdf_slider').flexslider({
	    	animation: "fade",
			controlNav: false,
			pauseOnHover: true,
	    	prevText: "<i class='fa fa-angle-left'></i>",
	    	nextText: "<i class='fa fa-angle-right'></i>",
	    	smoothHeight: true,
	    	multipleKeyboard: true
		});
		
	    /*================================= 
		CAROUSEL
		=================================*/

	    // Helper function to check the window size and return the amount of grid items that should be in the carousel
		
		function getGridSize() {
			const windowWidth = window.innerWidth;

			if (windowWidth > 768) {
				return 3;
			}
			else if(windowWidth < 768 && windowWidth >500) {
				return 2;
			}
			else {
				return 1;
			}
		}

	    $('.ihdf_post_carousel').flexslider({
	    	animation: "slide",
	    	controlNav: false,
	    	prevText: "<i class='fa fa-angle-left'></i>",
	    	nextText: "<i class='fa fa-angle-right'></i>",
	    	multipleKeyboard: true,
		    animationLoop: false,
		    itemWidth: 150,
		    slideshow: false,
		    maxItems: getGridSize()
	    });

	    // On window resize adjust the maxItems in the carousel depending on window width.

	    $(window).resize(function(){
	    	var carouselObject = $('.ihdf_post_carousel').data('flexslider');
	    	if(carouselObject) {
	    		carouselObject.vars.maxItems = getGridSize();
	    	}
	    });

	});

})(jQuery);