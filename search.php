<?php
/**
 * The template for displaying search results pages.
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page_header without_header_image">
				<div class="container">	
					<h1>Results for '<?php printf(get_search_query()); ?>'</h1>
				</div>
			</header>

			<div class="page_content">

				<div class="container">

					<?php while ( have_posts() ) : the_post(); ?>

						<div class="post_card_container">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<p class="wysiwyg"><?php the_excerpt(); ?></p>
						</div>

					<?php endwhile; ?>
				
				</div>

			</div>

			<?php ihdf_pagination($wp_query->max_num_pages); ?>

		<?php else : ?>

			<header class="page_header without_header_image">
				<div class="container">	
					<h1>No Results Found</h1>
				</div>
			</header>

		<?php endif; ?>

	</main>

<?php get_footer(); ?>
