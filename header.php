<?php
/**
 * Theme header
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicon -->

<link rel="icon" type="img/png" href="" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header class="main_header">
		<div class="container">
			<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img src="<?php the_field('ihdf_logo', 'options'); ?>" />
			</a>
			<nav class="main_navigation">
				<?php wp_nav_menu( array( 'menu' => 'main-menu', 'menu_id' => 'primary-menu', 'menu_class' => 'main_menu', 'container' => '' ) ); ?>
				<ul class="main_navigation_ctas">
					<?php if(get_field('donation_page_link', 'options')): ?>
						<li>
							<a href="<?php the_field('donation_page_link', 'options'); ?>" class="ihdf_button magenta"><i class="fa fa-heart-o"></i> <span>Donate</span></a>
						</li>
					<?php endif; ?>
					<li>
						<i class="fa fa-search search_toggle"></i>
					</li>
					<li>
						<a href="mailto:<?php the_field('ihdf_email', 'options'); ?>"><i class="fa fa-envelope"></i></a>
					</li>
				</ul>
			</nav>
		</div>

		<!-- Search Bar -->
	
		<section class="ihdf_search">
			<?php get_search_form(); ?>
		</section>
		
	</header>

	<!-- Mobile Header -->

	<header class="mobile_header">
		<div class="container">
			<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img src="<?php the_field('ihdf_logo', 'options'); ?>" />
			</a>
			<div class="mobile_header_right">
				<?php if(get_field('donation_page_link', 'options')): ?>
					<a href="<?php the_field('donation_page_link', 'options'); ?>" class="ihdf_button magenta"><i class="fa fa-heart-o"></i> <span>Donate</span></a>
				<?php endif; ?>
				<span class="menu_icon"></span>
			</div>
		</div>
	</header>

	<!-- Mobile Navigation -->

	<nav class="mobile_navigation">
		<?php wp_nav_menu( array( 'menu' => 'main-menu', 'menu_id' => 'primary-menu', 'container' => '' ) ); ?>
	</nav>

	<!-- Donate Floater CTA -->

	<?php if(get_field('donation_page_link', 'options')): ?>
		<a href="<?php the_field('donation_page_link', 'options'); ?>" class="donate_floater">
			<i class="fa fa-heart-o"></i>
		</a>
	<?php endif; ?>

	<!-- Loading Spinner -->

	<div class="loading_spinner"></div>

	<div id="content" class="main_content">