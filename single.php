<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<div class="single_post_top_social">
					<?php get_template_part('template-parts/social_share_bar'); ?>
				</div>

				<!-- Title/Meta -->

				<div class="container">

					<header class="single_post_header">

						<h2><?php the_title(); ?></h2>

						<?php get_template_part('template-parts/post_meta'); ?>

					</header>

				</div>

				<!-- Content -->

				<div class="wysiwyg">
					<div class="small_container">
						<?php the_content(); ?>
					</div>
				</div>

				<div class="container">

					<!-- Footer -->

					<footer class="single_post_footer">

						<!-- Tags -->

						<div class="single_post_tags">
							<?php 
								$post_terms = get_the_terms($post->ID, 'post_tag');
								// Colors Array
								$tag_colors = ['teal', 'purple', 'magenta', 'lime'];
								$post_terms_list_array = array();
								if($post_terms && ! is_wp_error($post_terms)):
									foreach($post_terms as $term) {
										// Random Tag Color
										$random_number = rand(0,3);
										//	Add each tag to our tags array
										$post_terms_list_array[] = '<a href="' . get_term_link($term->term_id) . '" class="ihdf_button ' . $tag_colors[$random_number] . '">' . $term->name . '</a>'; 
									}
									echo implode(' ', $post_terms_list_array);
								endif;
							?>
						</div>

						<!-- Social Share -->

						<div class="social_share_bar">
							<ul class="social_icons flush_right">
								<li>Share</li>
								<li><a href="<?php the_permalink(); ?>" class="fb_share"><i class="fa fa-facebook"></i></a></li>
								<li><a href="<?php the_permalink(); ?>" class="twitter_share" data-title="<?php the_title(); ?>"><i class="fa fa-twitter"></i></a></li>
								<li><a href="<?php the_permalink(); ?>" class="linkedin_share" data-title="<?php the_title(); ?>"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="mailto:?subject=<?php echo str_replace(' ', '%20', get_the_title()); ?>&body=<?php the_permalink(); ?>"><i class="fa fa-envelope"></i></a></li>
							</ul>
						</div>

					</footer>

				</div>
				
			</div>

		<?php endwhile; ?>

	</main>
	
<?php get_footer(); ?>