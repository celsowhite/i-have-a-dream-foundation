<?php
/**
 * The template for displaying 404 pages (not found).
 */

get_header(); ?>

	<main class="main_wrapper">
	
		<header class="page_header without_header_image">
			<div class="container">	
				<h1>Page Not Found</h1>
			</div>
		</header>

	</main>

<?php get_footer(); ?>
