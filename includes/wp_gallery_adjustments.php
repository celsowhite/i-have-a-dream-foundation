<?php 

/*==========================================
Default Gallery 
Update the default gallery markup to work with Magnific Popup
==========================================*/

// Function to output the correct gallery markup

function default_gallery_transformation($atts) {
    
    // Get the ID's of the images in the gallery

    $image_ids = explode(',', $atts['ids']);

    // Columns
    // If columns are set then make sure the grid container has the appropriate class name.
    // Defaults to three.

    $columns_name = 'three_columns';

    if(isset($atts['columns'])) {

        $columns = $atts['columns'];

        if ($columns === '1') {
            $columns_name = 'one_column';
        } 
        elseif ($columns === '2') {
            $columns_name = 'two_columns';
        } 
        elseif ($columns === '3') {
            $columns_name = 'three_columns';
        } 
        elseif ($columns === '4') {
            $columns_name = 'four_columns';
        } 
        elseif ($columns == '5') {
            $columns_name = 'five_columns';
        }
        elseif ($columns == '6') {
            $columns_name = 'six_columns';
        }
        elseif ($columns == '7') {
            $columns_name = 'seven_columns';
        }
        elseif ($columns == '8') {
            $columns_name = 'eight_columns';
        }
        elseif ($columns == '9') {
            $columns_name = 'nine_columns';
        }

    }

    // Start the html markup for the slider

    $gallery_output = '<div class="wp_custom_gallery ' . $columns_name . '">';

    // Loop through each image ID to get it's attachment URL to add to our html markup

    foreach($image_ids as $id) {
        $full_image_src = wp_get_attachment_image_src($id, 'large')[0];
        $thumbnail_image_src = wp_get_attachment_image_src($id, 'large_thumbnail')[0];
        if(isset(get_post($id)->post_excerpt)) {
            $image_caption = htmlspecialchars(get_post($id)->post_excerpt);
        }
        $gallery_output .= '<a href="' . $full_image_src . '" title="' . $image_caption . '"><div class="wp_custom_gallery_thumbnail"><img src="' . $thumbnail_image_src . '" /></div></a>';
    }

    // Finalize the output by closing out our markup and returning it to the post_gallery filter hook

    return $gallery_output . '</div>';
} 

/*==========================================
Post Gallery Filter Hook
==========================================*/

function gallery_shortcode_adjustment( $output = '', $atts, $instance ) {
    
    $return = $output;

    $gallery_markup = default_gallery_transformation($atts);

    if(!empty( $gallery_markup)) {
        $return = $gallery_markup;
    }

    return $return;
}

add_filter( 'post_gallery', 'gallery_shortcode_adjustment', 10, 3 );

?>