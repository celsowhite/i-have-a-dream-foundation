<?php

/*==========================================
QUOTES
==========================================*/

function ihdf_quote_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(
        'citation' => '',
        'color'    => 'purple',
        'width'    => 'normal'
    ), $atts );

    if($a['width'] === 'full') {
        return '</div><div class="container"><div class="ihdf_quote_container ' . $a['color'] . '">' . '<h2 class="museo_slab">' . $content . '</h2><h4 class="ihdf_quote_citation">' . $a['citation'] . '</h4></div></div><div class="small_container">';
    }
    else {
        return '<div class="ihdf_quote_container ' . $a['color'] . '">' . '<h2 class="museo_slab">' . $content . '</h2><h4 class="ihdf_quote_citation">' . $a['citation'] . '</h4></div>';
    }

}

add_shortcode( 'ihdf_quote', 'ihdf_quote_shortcode' );

/*==========================================
TWEET QUOTES
==========================================*/

function tweet_this_shortcode( $atts, $content = null ) {

	return '<h3 class="museo_slab">' . $content . '</h3><a class="tweet_this underlined_link" data-text="' . $content . '" href="">Click To Tweet</a>';

}

add_shortcode( 'tweet_this', 'tweet_this_shortcode' );

/*==========================================
IHDF BUTTON
==========================================*/

function ihdf_button_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(
        'url'     => '',
        'color'   => 'purple',
        'type'    => 'normal',
        'new_tab' => 'false'
    ), $atts );

    if($a['new_tab'] === 'true') {
        $new_tab = 'target="_blank"';
    }
    else {
        $new_tab = '';
    }

    if($a['type'] === 'donate'){
        return '<a href="' . $a['url'] . '"' . $new_tab . 'class="ihdf_button magenta"><i class="fa fa-heart"></i> ' . $content . '</a>';
    }
    elseif($a['type'] === 'download') {
        return '<a href="' . $a['url'] . '"' . $new_tab . 'class="ihdf_button ' . $a['color'] . '"><i class="fa fa-download"></i> ' . $content . '</a>';
    }
    else {
        return '<a href="' . $a['url'] . '"' . $new_tab . 'class="ihdf_button ' . $a['color'] . '">' . $content . '</a>';
    }

}

add_shortcode( 'ihdf_button', 'ihdf_button_shortcode' );

/*==========================================
DOWNLOAD BUTTON
==========================================*/

function download_button_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(
        'url'   => '',
        'color' => 'purple'
    ), $atts );

	return '<a href="' . $a['url'] . '" class="ihdf_button ' . $a['color'] . '"><i class="fa fa-download"></i> ' . $content . '</a>';

}

add_shortcode( 'download_button', 'download_button_shortcode' );

/*==========================================
SHARE THIS BAR
==========================================*/

function share_this_bar_shortcode($atts, $content = null) {

    $a = shortcode_atts( array(
        'fb_link'      => '',
        'twitter_text' => ''
    ), $atts);

    $bar_string = '<ul class="share_this_bar">'
                . '<li><a class="fb_share" href="' . $a['fb_link'] . '"><i class="fa fa-facebook"></i> Share on facebook</a></li>'
                . '<li><a class="tweet_this" data-text="' . $a['twitter_text'] . '"><i class="fa fa-twitter"></i> Share on twitter</a></li>'
                . '<li><a href="http://eepurl.com/bG6ltT" target="_blank"><i class="fa fa-envelope"></i> Sign up for our newsletter</a></li>'
                . '</ul>';
    
    return $bar_string;

}

add_shortcode('share_this_bar', 'share_this_bar_shortcode');

/*==========================================
EXPAND CONTAINER
==========================================*/

function expand_container_shortcode( $atts, $content = null ) {

    return '</div><div class="container">' . $content . '</div><div class="small_container">';

}

add_shortcode( 'expand_container', 'expand_container_shortcode' );

/*==========================================
IMAGE HOVER EFFECT
User can add two image src's to this shortcode to get the image hover effect. Hovering on
the main image reveals the hovered image.
==========================================*/

function ihdf_image_hover_shortcode( $atts, $content = null ) {

    $a = shortcode_atts( array(
        'main_image_url'  => '',
        'hover_image_url' => ''
    ), $atts);

    $image_hover_element = '<div class="hover_image_container">'
                         . '<img class="static" src="' . $a['main_image_url'] . '" />'
                         . '<img class="hover" src="' . $a['hover_image_url'] . '" />'
                         . '</div>';

    return $image_hover_element;

}

add_shortcode( 'ihdf_image_hover', 'ihdf_image_hover_shortcode' );

?>