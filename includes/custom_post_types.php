<?php

/*==========================================
POST
==========================================*/

// Post Author Taxonomy

function add_post_author_taxonomy() {
	$labels = array(
		'name' => ('Author'),
      	'singular_name' => ('Author'),
      	'search_items' =>  ('Search Authors' ),
      	'all_items' => ('All Authors' ),
      	'parent_item' => ('Parent Author' ),
      	'parent_item_colon' => ('Parent Author:' ),
      	'edit_item' => ('Edit Author' ),
      	'update_item' => ('Update Author' ),
      	'add_new_item' => ('Add New Author' ),
      	'new_item_name' => ('New Author' ),
      	'menu_name' => ('Authors' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'rewrite'           => array( 'slug' => 'authors'),
		'query_var'         => true
	);

	register_taxonomy( 'ihdf_post_author', array('post'), $args );
}

add_action( 'init', 'add_post_author_taxonomy', 0 );

// Post Affiliate Taxonomy

function add_post_affiliate_taxonomy() {
	$labels = array(
		'name' => ('Affiliate'),
      	'singular_name' => ('Affiliate'),
      	'search_items' =>  ('Search Affiliates' ),
      	'all_items' => ('All Affiliates' ),
      	'parent_item' => ('Parent Affiliate' ),
      	'parent_item_colon' => ('Parent Affiliate:' ),
      	'edit_item' => ('Edit Affiliate' ),
      	'update_item' => ('Update Affiliate' ),
      	'add_new_item' => ('Add New Affiliate' ),
      	'new_item_name' => ('New Affiliate' ),
      	'menu_name' => ('Affiliates' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'rewrite'           => array( 'slug' => 'affiliates'),
		'query_var'         => true
	);

	register_taxonomy( 'ihdf_post_affiliate', array('post'), $args );
}

add_action( 'init', 'add_post_affiliate_taxonomy', 0 );

/*==========================================
TEAM
==========================================*/

// Post Type

function custom_post_type_team() {

	$labels = array(
		'name'                => ('Team'),
		'singular_name'       => ('Team'),
		'menu_name'           => ('Team'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Team'),
		'view_item'           => ('View Team'),
		'add_new_item'        => ('Add New Team'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Team'),
		'update_item'         => ('Update Team'),
		'search_items'        => ('Search Team'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('team'),
		'description'         => ('IHDF team and board members'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'team'),
		'taxonomies'          => array('ihdf_team_type'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 20,
		'menu_icon'  		  => 'dashicons-groups',
	);

	register_post_type( 'ihdf_team', $args );

}

add_action( 'init', 'custom_post_type_team', 0 );

// Team Type Taxonomy

function add_team_type_taxonomy() {
	$labels = array(
		'name' => ('Type'),
      	'singular_name' => ('Type'),
      	'search_items' =>  ('Search Types' ),
      	'all_items' => ('All Types' ),
      	'parent_item' => ('Parent Type' ),
      	'parent_item_colon' => ('Parent Type:' ),
      	'edit_item' => ('Edit Type' ),
      	'update_item' => ('Update Type' ),
      	'add_new_item' => ('Add New Type' ),
      	'new_item_name' => ('New Type' ),
      	'menu_name' => ('Types' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'query_var'         => true
	);

	register_taxonomy( 'ihdf_team_type', array('ihdf_team'), $args );
}

add_action( 'init', 'add_team_type_taxonomy', 0 );

?>