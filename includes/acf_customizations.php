<?php 

// Customize ACF path

add_filter('acf/settings/path', 'my_acf_settings_path');
 
function my_acf_settings_path( $path ) {
 
    $path = get_template_directory() . '/acf/';
    return $path;
    
}
 
// Customize ACF directory

add_filter('acf/settings/dir', 'my_acf_settings_dir');
 
function my_acf_settings_dir( $dir ) {
 
    $dir = get_template_directory_uri() . '/acf/';
    return $dir;
    
}

// Always save ACF json to parent theme

add_filter('acf/settings/save_json', 'my_acf_json_save_point');
 
function my_acf_json_save_point( $path ) {
    
    $path = get_template_directory() . '/acf-json';
    return $path;
    
}

// Always load ACF json from parent theme

add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {
    
    unset($paths[0]);    
    $paths[] = get_template_directory() . '/acf-json';
    return $paths;
    
}

// Hide ACF field group menu item only if not on localhost

$site_url = site_url();

if (strpos($site_url, 'localhost') === false) {
    add_filter('acf/settings/show_admin', '__return_false');
}

// Include ACF

include_once( get_template_directory() . '/acf/acf.php' );

// Show Options Page

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

?>