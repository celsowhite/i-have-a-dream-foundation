<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
*/

?>

</div>

<footer class="footer">

	<?php 
	
	// Show email newsletter on every page except for the homepage and donate.

	if(!is_front_page() && get_the_title() !== 'Donate'): ?>
		
		<?php get_template_part('template-parts/ihdf_newsletter'); ?>

	<?php endif; ?>

	<section class="footer_top">

		<div class="container">

			<ul class="social_icons">
				<?php if(get_field('ihdf_facebook', 'options')): ?>
					<li><a href="<?php the_field('ihdf_facebook', 'options'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
				<?php endif; ?>
				<?php if(get_field('ihdf_twitter', 'options')): ?>
					<li><a href="<?php the_field('ihdf_twitter', 'options'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<?php endif; ?>
				<?php if(get_field('ihdf_linkedin', 'options')): ?>
					<li><a href="<?php the_field('ihdf_linkedin', 'options'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
				<?php endif; ?>
				<?php if(get_field('ihdf_instagram', 'options')): ?>
					<li><a href="<?php the_field('ihdf_instagram', 'options'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
				<?php endif; ?>
			</ul>

			<?php wp_nav_menu( array( 'menu' => 'footer-primary-menu', 'menu_class' => 'footer_menu footer_primary_menu', 'container' => '' ) ); ?>

			<?php wp_nav_menu( array( 'menu' => 'footer-secondary-menu', 'menu_class' => 'footer_menu footer_secondary_menu', 'container' => '' ) ); ?>

		</div>

	</section>

	<section class="footer_bottom">

		<div class="container">

			<p class="footer_address_container"><?php if(get_field('ihdf_address', 'options')): ?><span class="address"><?php the_field('ihdf_address', 'options'); ?></span><?php endif; ?><?php if(get_field('ihdf_telephone', 'options')): ?><span class="telephone">T: <?php the_field('ihdf_telephone', 'options'); ?></span><?php endif; ?></p>

			<p>© “I Have A Dream” Foundation <?php echo date('Y'); ?></p>

		</div>

	</section>

</footer>

<?php wp_footer(); ?>

<?php 

// Google Analytics Tracking

if(get_field('google_analytics_id', 'options')): 
$google_analytics_id = get_field('google_analytics_id', 'options');
?>
	
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $google_analytics_id; ?>"></script>

<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', '<?php echo $google_analytics_id; ?>');
</script>

<?php endif; ?>

</body>
</html>
