<?php
/**
 * The template for displaying archive pages.
 */

get_header(); ?>
	
	<main class="main_wrapper">

		<header class="page_header without_header_image">
			<div class="container">	
				<h1><?php single_term_title(); ?></h1>
			</div>
		</header>

		<div class="page_content">

			<div class="container">

				<?php while ( have_posts() ) : the_post(); ?>
				
					<?php get_template_part('template-parts/blog_post_card'); ?>

				<?php endwhile; ?>

				<?php ihdf_pagination($wp_query->max_num_pages); ?>

			</div>

		</div>

	</main>

<?php get_footer(); ?>