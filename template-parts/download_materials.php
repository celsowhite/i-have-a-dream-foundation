<?php if(have_rows('materials')): ?>

	<section class="ihdf_panel purple_to_teal_gradient">

		<h2 class="white_text text_center">Download Our Materials</h2>

		<ul class="centered_list">

			<?php while(have_rows('materials')): the_row(); ?>

				<li><a href="<?php the_sub_field('file'); ?>" target="_blank" class="ihdf_button white"><i class="fa fa-download"></i> <?php the_sub_field('name'); ?></a></li>

			<?php endwhile; ?>

		</ul>

	</section>

<?php endif; ?>