<section class="ihdf_get_involved_component ihdf_panel_padding_top">
	<div class="container">
        <h2><?php the_field('newsletter_header', 'option'); ?></h2>
        <div class="ihdf_row">
            <div class="column_1_2 text_center">
                <p>Be a Dream Maker.</p>
                <a href="<?php echo get_page_link(16); ?>" class="ihdf_button magenta"><i class="fa fa-heart-o"></i> Donate</a>
            </div>
            <div class="column_1_2 text_center">
                <p>Invest in a brighter future.</p>
                <a href="<?php echo get_page_link(55); ?>" class="ihdf_button teal">Start A Program</a>
            </div>
        </div>
	</div>
</section>