<?php 
// Social Share Bar
// Used across posts and pages to share IHDF content
?>

<section class="social_share_bar">
	<div class="container">
		<ul class="social_icons flush_right">
			<li><a href="<?php the_permalink(); ?>" class="fb_share"><i class="fa fa-facebook"></i></a></li>
			<li><a href="<?php the_permalink(); ?>" class="twitter_share" data-title="<?php the_title(); ?>"><i class="fa fa-twitter"></i></a></li>
			<li><a href="<?php the_permalink(); ?>" class="linkedin_share" data-title="<?php the_title(); ?>"><i class="fa fa-linkedin"></i></a></li>
			<li><a href="mailto:?subject=<?php echo str_replace(' ', '%20', get_the_title()); ?>&body=<?php the_permalink(); ?>"><i class="fa fa-envelope"></i></a></li>
		</ul>
	</div>
</section>