<?php
	// Masonry showcase component.
	// Includes 4 images with quotes and one featured video.

	$masonry_images = get_field('masonry_showcase_images');
?>

<?php 
// Optional Title
if(get_field('masonry_showcase_title')): ?>
	<h2 class="text_center"><?php the_field('masonry_showcase_title'); ?></h2>
<?php endif; ?>

<section class="ihdf_masonry <?php if(get_field('masonry_showcase_video')): ?>with_video<?php endif; ?>">
	<div class="outer_grid_column1 masonry_item" style="background-image:url('<?php echo $masonry_images[0]['image'] ?>')">
		<div class="item_overlay">
			<div class="item_overlay_content">
				<h1 class="museo_slab"><?php echo $masonry_images[0]['quote'] ?></h1>
				<h4>- <?php echo $masonry_images[0]['citation'] ?></h4>
			</div>
		</div>
	</div>
	<div class="outer_grid_column2">
		<div class="inner_grid_row top">
			<div class="masonry_item" style="background-image:url('<?php echo $masonry_images[1]['image'] ?>')">
				<div class="item_overlay">
					<div class="item_overlay_content">
						<h3 class="museo_slab"><?php echo $masonry_images[1]['quote'] ?></h3>
						<h4>- <?php echo $masonry_images[1]['citation'] ?></h4>
					</div>
				</div>
			</div>
			<div class="masonry_item" style="background-image:url('<?php echo $masonry_images[2]['image'] ?>')">
				<div class="item_overlay">
					<div class="item_overlay_content">
						<h3 class="museo_slab"><?php echo $masonry_images[2]['quote'] ?></h3>
						<h4>- <?php echo $masonry_images[2]['citation'] ?></h4>
					</div>
				</div>
			</div>
		</div>
		<div class="inner_grid_row bottom">
			<?php 
			// Bottom Row w/ Video
			if(get_field('masonry_showcase_video')): ?>
				<?php echo get_field('masonry_showcase_video'); ?>
				<div class="masonry_item" style="background-image:url('<?php echo $masonry_images[3]['image'] ?>')">
					<div class="item_overlay">
						<div class="item_overlay_content">
							<h3 class="museo_slab"><?php echo $masonry_images[3]['quote'] ?></h3>
							<h4>- <?php echo $masonry_images[3]['citation'] ?></h4>
						</div>
					</div>
				</div>
			<?php 
			// Bottom Row without video
			else: ?>
				<div class="masonry_item" style="background-image:url('<?php echo $masonry_images[3]['image'] ?>')">
					<div class="item_overlay">
						<div class="item_overlay_content">
							<h3 class="museo_slab"><?php echo $masonry_images[3]['quote'] ?></h3>
							<h4>- <?php echo $masonry_images[3]['citation'] ?></h4>
						</div>
					</div>
				</div>
				<div class="masonry_item" style="background-image:url('<?php echo $masonry_images[4]['image'] ?>')">
					<div class="item_overlay">
						<div class="item_overlay_content">
							<h3 class="museo_slab"><?php echo $masonry_images[4]['quote'] ?></h3>
							<h4>- <?php echo $masonry_images[4]['citation'] ?></h4>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
