<?php
	// Header component for posts and pages
	// IDHF can eigther have a header image with title overlay or just a title
?>

<?php if(get_field('header_image')): ?>

	<header class="page_header" style="background-image: url(<?php the_field('header_image'); ?>); background-position: <?php the_field('header_image_position'); ?>">
		<div class="page_header_content">
			<div class="container">
				<h1 style="background-color: <?php the_field('header_image_title_background_color'); ?>"><?php the_field('header_image_title'); ?></h1>
			</div>
		</div>
	</header>

<?php elseif(!is_single()): ?>

	<header class="page_header without_header_image">
		<div class="container">	
			<h1><?php the_title(); ?></h1>
		</div>
	</header>

<?php endif; ?>