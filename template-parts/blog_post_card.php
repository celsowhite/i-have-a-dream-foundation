<?php 
// Blog post component with image, title, meta and description.
// Used on category pages.
?>

<div class="ihdf_row post_card_container">
	<div class="column_1_3">
		<?php the_post_thumbnail('square_thumbnail'); ?>
	</div>
	<div class="column_2_3">
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php get_template_part('template-parts/post_meta'); ?>
		<?php the_excerpt(); ?>
		<a href="<?php the_permalink(); ?>" class="ihdf_button purple">Read More</a>
	</div>
</div>