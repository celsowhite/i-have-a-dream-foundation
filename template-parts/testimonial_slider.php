<?php 
// Testimonial Slider Component
// Slides can be configured with a background image and without
?>

<?php if(have_rows('testimonials')): ?>

	<?php if(get_field('testimonial_header') && !is_page('homepage')): ?>
		<h2 class="text_center testimonial_header"><?php the_field('testimonial_header'); ?></h2>
	<?php endif; ?>

	<div class="flexslider ihdf_slider testimonial_slider">
		<ul class="slides">
			<?php while(have_rows('testimonials')): the_row(); ?>
				
				<?php 

				// Image slide

				if(get_sub_field('slide_type') === 'image'): ?>

					<li class="with_background_image" style="background-image: url(<?php the_sub_field('background_image'); ?>);"></li>

				<?php 

				// Quote slide

				else: ?>

					<?php 
					// Testimonial w/ Background Image
					if(get_sub_field('background_image_selection')): ?>
						<li class="<?php the_sub_field('background_gradient'); ?> with_background_image" style="background-image: url(<?php the_sub_field('background_image'); ?>);">
							<div class="testimonial_content_container">
								<div class="container <?php the_sub_field('text_positioning'); ?>">
									<div class="testimonial_content">
										<h2 class="museo_slab"><?php the_sub_field('quote'); ?></h2>
										<h4><?php the_sub_field('citation'); ?></h4>
									</div>
								</div>
							</div>
						</li>
						
					<?php 
					// Testimonial w/o Background Image
					else: ?>
						<li class="<?php the_sub_field('background_gradient'); ?>">
							<div class="testimonial_content_container">
								<div class="container">
									<div class="testimonial_content">
										<h2 class="museo_slab"><?php the_sub_field('quote'); ?></h2>
										<h4><?php the_sub_field('citation'); ?></h4>
									</div>
								</div>
							</div>
						</li>
					<?php endif; ?>

				<?php endif; ?>
			<?php endwhile; ?>
		</ul>

		<?php 
		// Only include a submit a dream button on the homepage testimonial slider.
		if($testimonial_slider_with_submit_button): ?>
		<div class="container "></div>
			<div class="testimonial_slider_header">
				<div class="container">
					<h2><?php the_field('testimonial_header'); ?></h2>
				</div>
			</div>
			<a class="ihdf_button white submit_your_dream">Submit Your Dream</a>
		<?php endif; ?>
		
	</div>

<?php endif; ?>