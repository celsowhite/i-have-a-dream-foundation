<?php 

/*---------------------- 
Post meta information.
---
Conditionally shows information about the post depending on the sites settings.
Must be used within a post loop.
----------------------*/

// Save an array of this posts categories.
$categories = get_the_category();

// Save an array of the categories that should not have a date display.
// Assigned in the backend using ACF via the options page.
$categories_without_date_display = get_field('categories_without_date_display', 'options');

// Date display flag. By default show the date.
$show_post_date = true;

// Loop through each of the posts categories to check if they are in our array of categories that shouldn't
// have a date display.

if($categories_without_date_display) {
    foreach( $categories as $category ) {
        if(in_array($category->term_id, $categories_without_date_display)) {
            $show_post_date = false;
        }
    }
}

?>

<div class="post_meta">
    <h4>
        <?php if(get_the_terms($post->ID, 'ihdf_post_author')): ?>
            By <?php echo category_terms_list($post->ID, 'ihdf_post_author'); ?>
        <?php endif; ?> 
        <?php if(get_the_terms($post->ID, 'ihdf_post_author') && $show_post_date): ?>
            |
        <?php endif; ?>
        <?php if($show_post_date): ?>
            <?php echo get_the_date(); ?>
        <?php endif; ?>
    </h4>
</div>