<section class="ihdf_newsletter ihdf_panel" id="ihdf_newsletter_container">
	<div class="container newsletter_content">
		<p><?php the_field('newsletter_description', 'option'); ?></p>
		<?php
		// Save the embed type so we can wrap the embed in the appropriate class and style the form depending on which provider is being used.
		$embed_type = get_field('newsletter_embed_type', 'option');
		$embed_container_class;
		if( $embed_type === 'mailchimp' ) {
			$embed_container_class = 'ihdf_mailchimp_embed';
		}	
		elseif ($embed_type === 'constantContact') {
			$embed_container_class = 'ihdf_constant_contact_embed';
		}
		else {
			$embed_container_class = 'ihdf_other_embed';
		}
		?>
		<div class="<?php echo $embed_container_class; ?>">
			<?php the_field('newsletter_embed', 'option'); ?>
		</div>
	</div>
</section>