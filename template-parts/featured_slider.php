<?php 
// Featured Posts Slider Component
?>

<div class="flexslider featured_slider ihdf_slider">
	<ul class="slides">
		<?php if(have_rows('featured_slides')): while(have_rows('featured_slides')): the_row(); ?>
			
			<?php 

			// Image slide

			if(get_sub_field('slide_type') === 'image'): ?>

				<li class="with_background_image" style="background-image: url(<?php the_sub_field('image'); ?>)">
					<div class="slide_content_container">
						<div class="container <?php the_sub_field('text_position'); ?>">
							<div class="slide_content">
								<div class="title_container">
									<h1 style="background-color: <?php the_sub_field('highlight_color'); ?>"><?php the_sub_field('title'); ?></h1>
								</div>
								<?php if(get_sub_field('description')): ?>
									<p style="background-color: <?php the_sub_field('highlight_color'); ?>" class="description"><?php the_sub_field('description'); ?></p>
								<?php endif; ?>
								<p><a href="<?php the_sub_field('link'); ?>" class="ihdf_solid_button" style="background-color: <?php the_sub_field('highlight_color'); ?>">Learn More</a></p>
							</div>
						</div>
					</div>
				</li>

			<?php 

			// Video Slide

			else: ?>

				<li>
					<div data-type="<?php the_sub_field('video_type'); ?>" data-video-id="<?php the_sub_field('video_embed', false, false); ?>"></div>
				</li>

			<?php endif; ?>
			
		<?php endwhile; endif; ?>
	</ul>
</div>