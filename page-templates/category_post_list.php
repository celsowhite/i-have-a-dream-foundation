<?php
/*
Template Name: Category
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<div class="container">
					<div class="wysiwyg">
						<?php the_content(); ?>
                    </div>
                    
					<!-- Category List -->

					<?php
					$cat_loop_args = array (
						'post_type'       => 'post', 
						'cat'             => get_field('page_category'), 
						'posts_per_page'  => 10,
						'paged'           => $paged,
						'order'           => 'DESC'
					);
					$cat_loop = new WP_Query($cat_loop_args);
					if ($cat_loop -> have_posts()) : while ($cat_loop -> have_posts()) : $cat_loop -> the_post();
                    ?>	
                    
						<div class="post_card_container purple">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<?php get_template_part('template-parts/post_meta'); ?>
						    <?php the_excerpt(); ?>
		                    <a href="<?php the_permalink(); ?>" class="underlined_link">Read More</a>
                        </div>
                        
					<?php endwhile; wp_reset_postdata(); ?>

						<!-- Pagination -->

						<?php ihdf_pagination($cat_loop->max_num_pages); ?>

				 	<?php endif; ?>
				</div>
				
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
