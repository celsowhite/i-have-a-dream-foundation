<?php
/*
Template Name: Team
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<div class="container">

					<!-- Content -->

					<div class="wysiwyg">
						<?php the_content(); ?>
					</div>

					<!-- Loop of Team Members -->

					<?php
					$team_loop_args = array (
						'post_type'        => 'ihdf_team', 
						'tax_query'        => array(
							array(
								'taxonomy' => 'ihdf_team_type',
								'field'    => 'slug',
								'terms'    => array('board-member'),
								'operator' => 'NOT IN'
							)
						),
						'posts_per_page'   => -1,
						'paged'            => $paged,
						'order'            => 'ASC'
					);
					$team_loop = new WP_Query($team_loop_args);
					if ($team_loop -> have_posts()) : while ($team_loop -> have_posts()) : $team_loop -> the_post();
					?>	
						
						<div class="ihdf_row post_card_container purple">
							<div class="column_1_4">
								<?php if(get_field('team_hover_image')): ?>
									<div class="hover_image_container">
										<img class="static" src="<?php the_field('team_static_image'); ?>" />
										<img class="hover" src="<?php the_field('team_hover_image'); ?>" />
									</div>
								<?php else: ?>
									<img class="static" src="<?php the_field('team_static_image'); ?>" />
								<?php endif; ?>
							</div>
							<div class="column_3_4">
								<h3><?php the_title(); ?></h3>
								<?php if(get_field('team_title')): ?>
									<h4><?php the_field('team_title'); ?></h4>
								<?php endif; ?>
								<div class="wysiwyg">
									<?php the_content(); ?>
								</div>
							</div>
						</div>

					<?php endwhile; wp_reset_postdata(); ?>

						<!-- Pagination -->

						<?php ihdf_pagination($team_loop->max_num_pages); ?>

				 	<?php endif; ?>
				</div>
				
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
