<?php
/*
Template Name: Page w/ Sidebar
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<div class="container">
					<div class="ihdf_row">

						<!-- Sidebar Widgets -->

						<div class="column_1_3">
							<?php if(have_rows('sidebar_widgets')): while(have_rows('sidebar_widgets')): the_row(); ?>
								<div class="gradient_border_box">
									<?php the_sub_field('widget'); ?>
								</div>
							<?php endwhile; endif; ?>
						</div>

						<!-- Form -->
						
						<div class="column_2_3 wysiwyg">
							<?php the_content(); ?>
						</div>
					</div>
				</div>

				<!-- Testimonial Slider -->

				<?php get_template_part('template-parts/testimonial_slider'); ?>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>

