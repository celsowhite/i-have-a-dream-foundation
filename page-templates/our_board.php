<?php
/*
Template Name: Our Board
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<div class="container wysiwyg">

					<!-- Content -->

					<?php the_content(); ?>

					<!-- Loop of Board Members -->
					
					<?php
					$board_loop_args = array (
						'post_type'       => 'ihdf_team', 
						'ihdf_team_type'  => 'board-member', 
						'posts_per_page'  => -1,
						'paged'           => $paged,
						'order'           => 'ASC'
					);
					$board_loop = new WP_Query($board_loop_args);
					if ($board_loop -> have_posts()) : while ($board_loop -> have_posts()) : $board_loop -> the_post();
					?>	
						
						<div class="post_card_container" id="<?php echo $post->post_name ?>">
							<h3 class="lime_text"><?php the_title(); ?></h3>
							<?php if(get_field('team_title')): ?>
								<h4 class="lime_text"><?php the_field('team_title'); ?></h4>
							<?php endif; ?>
							<div class="read_more_container">
								<?php the_content(); ?>
							</div>
						</div>

					<?php endwhile; wp_reset_postdata(); endif; ?>

				</div>
				
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
