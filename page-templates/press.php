<?php
/*
Template Name: Press
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<div class="container">
					<div class="wysiwyg">
						<?php the_content(); ?>
					</div>
					<?php
					$press_loop_args = array (
						'post_type'       => 'post', 
						'category_name'   => 'press', 
						'posts_per_page'  => 20,
						'paged'           => $paged,
						'order'           => 'DESC',
						'orderby'         => 'meta_value',
						'meta_key'        => 'publication_date',
						'meta_type'       => 'DATETIME'
					);
					$press_loop = new WP_Query($press_loop_args);
					if ($press_loop -> have_posts()) : while ($press_loop -> have_posts()) : $press_loop -> the_post();
					?>	
						<div class="post_card_container purple">
							<h3><a href="<?php the_field('publication_link')?>" target="_blank"><?php the_title(); ?></a></h3>
							<h4><?php the_field('publication_name'); ?> | <?php the_field('publication_date'); ?></h4>
							<div class="wysiwyg">
								<?php the_content(); ?>
							</div>
							<a href="<?php the_field('publication_link')?>" target="_blank" class="underlined_link">View Article</a>
						</div>

					<?php endwhile; wp_reset_postdata(); ?>

						<!-- Pagination -->

						<?php ihdf_pagination($press_loop->max_num_pages); ?>

				 	<?php endif; ?>
				</div>
				
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
