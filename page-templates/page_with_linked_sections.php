<?php
/*
Template Name: Page w/ Linked Sections
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<div class="container">

                    <?php if ( !empty( get_the_content() ) ): ?>
     
                       <!-- Content -->

                        <div class="wysiwyg ihdf_panel_padding_bottom">
                            <?php the_content(); ?>
                        </div>

                    <?php endif; ?>

                    <!-- Sections Grid -->
                    
                    <div class="page_sections_grid">
                    
                        <div class="ihdf_row">
                            
                            <?php 
                            $grid_item_count = 0;
                            if(have_rows('page_sections')): while(have_rows('page_sections')): the_row();?>
                                <div class="column_1_4 page_sections_grid_block smooth_scroll" data-target="section-<?php echo $grid_item_count; ?>">
                                    <?php if(get_sub_field('hover_image')): ?>
                                        <div class="hover_image_container">
                                            <img class="static" src="<?php echo image_id_to_url(get_sub_field('image'), 'square_thumbnail'); ?>" />
                                            <img class="hover" src="<?php echo image_id_to_url(get_sub_field('hover_image'), 'square_thumbnail'); ?>" />
                                        </div>
                                    <?php else: ?>
                                        <img class="static" src="<?php echo image_id_to_url(get_sub_field('image'), 'square_thumbnail'); ?>" />
                                    <?php endif; ?>
                                    <h3 class="purple_text"><?php the_sub_field('title'); ?></h3>
                                    <?php if(get_sub_field('subtitle')): ?>
                                        <h4 class="purple_text"><?php the_sub_field('subtitle'); ?></h4>
                                    <?php endif; ?>
                                </div>
                            <?php $grid_item_count++; endwhile; endif; ?>

                        </div>
                    
                    </div>

					<!-- List Sections -->

                    <?php 
                    $section_item_count = 0;
                    if(have_rows('page_sections')): while(have_rows('page_sections')): the_row(); ?>

                        <div class="ihdf_row post_card_container" id="section-<?php echo $section_item_count; ?>">
                            <div class="column_1_4">
                                <?php if(get_sub_field('hover_image')): ?>
                                    <div class="hover_image_container">
                                        <img class="static" src="<?php echo image_id_to_url(get_sub_field('image'), 'square_thumbnail'); ?>" />
                                        <img class="hover" src="<?php echo image_id_to_url(get_sub_field('hover_image'), 'square_thumbnail'); ?>" />
                                    </div>
                                <?php else: ?>
                                    <img class="static" src="<?php echo image_id_to_url(get_sub_field('image'), 'square_thumbnail'); ?>" />
                                <?php endif; ?>
                            </div>
                            <div class="column_3_4">
                                <?php 
                                // If this section has a link then show the read more button and link the title.
                                if(get_sub_field('link')): ?>
                                    <h3 class="purple_text"><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a></h3>
                                    <?php if(get_sub_field('subtitle')): ?>
                                        <h4 class="purple_text"><?php the_sub_field('subtitle'); ?></h4>
                                    <?php endif; ?>
                                    <?php the_sub_field('excerpt'); ?>
                                    <a href="<?php the_sub_field('link'); ?>" class="ihdf_button purple">Learn More</a>
                                <?php else: ?>
                                    <h3 class="purple_text"><?php the_sub_field('title'); ?></h3>
                                    <?php if(get_sub_field('subtitle')): ?>
                                        <h4 class="purple_text"><?php the_sub_field('subtitle'); ?></h4>
                                    <?php endif; ?>
                                    <?php the_sub_field('excerpt'); ?>
                                <?php endif; ?>
                            </div>
                        </div>

                    <?php $section_item_count++; endwhile; endif; ?>

                </div>

                <!-- Testimonial Slider -->

				<?php get_template_part('template-parts/testimonial_slider'); ?>
				
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
