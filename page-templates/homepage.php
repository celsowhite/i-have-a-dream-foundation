<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Featured Posts Slider -->

			<?php get_template_part('template-parts/featured_slider'); ?>

			<div class="page_content">

				<div class="container wysiwyg">
					<?php the_content(); ?>
				</div>

				<!-- Masonry Testimonials -->

				<?php get_template_part('template-parts/masonry_showcase'); ?>

				<!-- Newsletter Signup -->

				<?php get_template_part('template-parts/ihdf_newsletter'); ?>

				<!-- Testimonial Slider -->

				<?php get_template_part('template-parts/testimonial_slider'); ?>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
